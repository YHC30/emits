import type { IuserInfo } from "@/interfaces/IUserInfo"

const Users: IuserInfo[] = [
  {
    id: 1,
    name: 'Aldo',
    lastName: 'Maldonado',
    email: 'Aldo@gmail.com',
    age: 24,
    msg: 'Esto es el Usuario 1'/* <!-- Cada vista debe tener algún mensaje que lo identifique al renderizarlo. --> */
  },
  {
    id: 2,
    name: 'Raquel',
    lastName: 'Ramos',
    email: 'rachel@gmail.com',
    age: 27,
    msg: 'Conocimientos en JS'
  },
  {
    id: 3,
    name: 'Olga',
    lastName: 'Olan',
    email: 'olgao@gmail.com',
    age: 26,
    msg: 'FullStart'
  },
  {
    id: 4,
    name: 'Lola',
    lastName: 'Lagunes',
    email: 'llagunes@gmail.com',
    age: 25,
    msg: 'Le sabe al diseño :)'
  },
  {
    id: 5,
    name: 'Karecio',
    lastName: 'Solis',
    email: 'Solito@gmail.com',
    age: 21,
    msg: 'Le gustan los michis'
  },
  {
    id: 6,
    name: 'Susana',
    lastName: 'Santos',
    email: 'ElSanto@gmail.com',
    age: 22,
    msg: 'El correo lo dice todo'
  }
]

export default Users