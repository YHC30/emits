import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
//import router from './router/index.js'; // Asegúrate de que la ruta sea correcta

createApp(App).mount('#app')
//createApp(App).use(router).mount('#app');